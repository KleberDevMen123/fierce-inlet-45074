class MusicsController < ApplicationController
  before_action :set_music, only: [:show, :edit, :update, :destroy]
  # before_action :set_img_music, only: [:create]

  # GET /musics
  # GET /musics.json
  def index
    @musics = Music.all
  end

  # GET /musics/1
  # GET /musics/1.json
  def show
  end

  # GET /musics/new
  def new
    @music = Music.new
  end

  # GET /musics/1/edit
  def edit
  end

  # def set_img_music
  #   if params[:music][:photo].present?
  #     if @music.photo.present?
  #       code_antigo = @user.cloudinary_code || "toauth_avatar_#{@user.cpf}"
  #       Cloudinary::Uploader.destroy(code_antigo)
  #     end
  #     @user.cloudinary_code = "toauth_avatar_#{@user.cpf}_#{DateTime.now.to_i}"
  #     Cloudinary::Uploader.upload(params[:user][:photo], public_id: "#{@user.cloudinary_code}", invalidate: true)
  #     @user.photo = Cloudinary::Utils.cloudinary_url("#{@user.cloudinary_code}", width: 100, height: 100, crop: :fill, gravity: :face)
  #     @user.save
  #   end
  #   redirect_to root_path
  # end

  # POST /musics
  # POST /musics.json
  def create
    @music = Music.new(music_params)

    # binding.pry
    #
    # if params[:photo].present?
    #   preloaded = Cloudinary::PreloadedFile.new(params[:photo])
    #   raise "Invalid upload signature" if !preloaded.valid?
    #   binding.pry
    #   @music.photo = preloaded.identifier
    # end

    respond_to do |format|
      if @music.save
        format.html {redirect_to @music, notice: 'Music was successfully created.'}
        format.json {render :show, status: :created, location: @music}
      else
        format.html {render :new}
        format.json {render json: @music.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /musics/1
  # PATCH/PUT /musics/1.json
  def update
    respond_to do |format|
      if @music.update(music_params)
        format.html {redirect_to @music, notice: 'Music was successfully updated.'}
        format.json {render :show, status: :ok, location: @music}
      else
        format.html {render :edit}
        format.json {render json: @music.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /musics/1
  # DELETE /musics/1.json
  def destroy
    @music.destroy
    respond_to do |format|
      format.html {redirect_to musics_url, notice: 'Music was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_music
    @music = Music.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def music_params
    params.require(:music).permit(:title, :artist, :lyric, :photo)
  end
end
