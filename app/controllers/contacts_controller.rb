class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy, :change_avatar]


  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.all
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html {redirect_to @contact, notice: 'Contact was successfully created.'}
        format.json {render :show, status: :created, location: @contact}
      else
        format.html {render :new}
        format.json {render json: @contact.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html {redirect_to @contact, notice: 'Contact was successfully updated.'}
        format.json {render :show, status: :ok, location: @contact}
      else
        format.html {render :edit}
        format.json {render json: @contact.errors, status: :unprocessable_entity}
      end
    end
  end

  def change_avatar
    # binding.pry
    if params[:contact][:avatar].present?
      if @contact.avatar.present?
        # code_antigo = @user.cloudinary_code || "toauth_avatar_#{@user.cpf}"
        # Cloudinary::Uploader.destroy(code_antigo)
        Cloudinary::Uploader.destroy(@contact.cloudinary_code)
      end
      # @user.cloudinary_code = "toauth_avatar_#{@user.cpf}_#{DateTime.now.to_i}"
      # Cloudinary::Uploader.upload(params[:user][:photo], public_id: "#{@user.cloudinary_code}", invalidate: true)
      public_id = Cloudinary::Uploader.upload(params[:contact][:avatar], public_id: "#{@contact.cloudinary_code}", invalidate: true)
      @contact.avatar_updated = Cloudinary::Utils.cloudinary_url(public_id["public_id"], width: 100, height: 100, crop: :fill)
      @contact.save
    end
    redirect_to @contact
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html {redirect_to contacts_url, notice: 'Contact was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_contact
    @contact = Contact.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require(:contact).permit(:description, :number, :email, :avatar)
  end
end
