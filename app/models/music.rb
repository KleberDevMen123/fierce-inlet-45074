class Music < ApplicationRecord
  def photo_url
    if self.photo.present?
      self.photo.sub! 'http://', 'https://'
    else
      'https://toauth.seplan.to.gov.br/mosaicpro/images/default-avatar.png'
    end
  end
end
