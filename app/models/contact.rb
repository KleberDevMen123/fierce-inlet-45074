class Contact < ApplicationRecord
  mount_uploader :avatar, AvatarUploader

  before_create :set_cloudinary_code

  def set_cloudinary_code
    self.cloudinary_code = self.avatar.public_id
  end


end
