class AddCloudinaryCodeToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :cloudinary_code, :string
  end
end
