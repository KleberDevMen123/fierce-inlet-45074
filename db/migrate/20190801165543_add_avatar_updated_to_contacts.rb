class AddAvatarUpdatedToContacts < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :avatar_updated, :string
  end
end
