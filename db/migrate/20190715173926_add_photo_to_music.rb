class AddPhotoToMusic < ActiveRecord::Migration[5.2]
  def change
    add_column :musics, :photo, :text
  end
end
