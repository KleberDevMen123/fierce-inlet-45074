# Load the Rails application.
require_relative 'application'

# Upload de imagens
require 'carrierwave/orm/activerecord'

# Initialize the Rails application.
Rails.application.initialize!
